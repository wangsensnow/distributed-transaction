package com.mine.homs.client.impl;

import com.mine.homs.client.TestClient;
import org.springframework.stereotype.Component;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@Component
public class TestClientHystrix implements TestClient {
    @Override
    public String getValue(Integer i) {
        return null;
    }

    @Override
    public String addForBreake(Integer i) {
        return "Feign也出错啦。。。";
    }
}
