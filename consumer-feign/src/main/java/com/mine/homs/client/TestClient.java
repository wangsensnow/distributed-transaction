package com.mine.homs.client;

import com.mine.homs.client.impl.TestClientHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@FeignClient(value = "PROVIDER-ONE",fallback = TestClientHystrix.class)
public interface TestClient {
    @GetMapping(value = "/test/getValue")
    String getValue(@RequestParam(value = "i") Integer i);

    @GetMapping(value = "/test/addForBreake")
    String addForBreake(@RequestParam(value = "i") Integer i);
}
