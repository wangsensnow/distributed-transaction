package com.mine.homs.controller;

import com.mine.homs.client.TestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@RestController
@RequestMapping(value = "/feign/test")
public class TestController {

    @Autowired
    TestClient testClient;
    @GetMapping(value = "/getValue")
    public String getValue(Integer i){
        return testClient.getValue(i);
    }
    @GetMapping(value = "/addForBreake")
    public String addForBreake(Integer i){
        return testClient.addForBreake(i);
    }
}
