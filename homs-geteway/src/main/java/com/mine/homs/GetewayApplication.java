package com.mine.homs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/8 23:06
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableZuulProxy
public class GetewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GetewayApplication.class);
    }
}
