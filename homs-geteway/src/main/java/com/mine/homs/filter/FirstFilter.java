package com.mine.homs.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/9 12:30
 */
@Component
public class FirstFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        StringBuffer url = request.getRequestURL();
        if(!url.toString().contains("getValue")){
            ctx.setSendZuulResponse(false);
            ctx.getResponse().setHeader("content-type", "text/html;charset=UTF-8");
            ctx.setResponseBody("没有getValue请求");
        }
        System.out.println(url);
        return null;
    }
}
