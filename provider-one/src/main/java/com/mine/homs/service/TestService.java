package com.mine.homs.service;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/2 0:02
 */
public interface TestService {
    String add(Integer i);
    String addForBreake(Integer i);
}
