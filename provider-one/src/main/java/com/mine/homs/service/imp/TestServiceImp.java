package com.mine.homs.service.imp;

import com.mine.homs.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@Service
public class TestServiceImp implements TestService {
    public final static Logger log = LoggerFactory.getLogger(TestServiceImp.class);
    @Override
    public String add(Integer i) {
        return String.valueOf(++i);
    }

    @Override
    public String addForBreake(Integer i) {
        System.out.println(22);
        throw new RuntimeException();
    }
}
