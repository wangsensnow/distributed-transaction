package com.mine.homs.controller;

import com.mine.homs.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/2 0:05
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    TestService testService;
    @GetMapping(value = "/getValue")
    public String getValue(Integer i)
    {
        return testService.add(i);
    }

    @GetMapping(value = "/addForBreake")
    public String addForBreake(Integer i) {
        System.out.println(2222);
        throw new RuntimeException();
    }
}
