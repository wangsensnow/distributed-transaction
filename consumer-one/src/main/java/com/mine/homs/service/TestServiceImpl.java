package com.mine.homs.service;

import com.mine.homs.service.impl.TestService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@Service
public class TestServiceImpl implements TestService {
    @Autowired
    RestTemplate template;
    @Override
    public String add(Integer i) {
        String url = "http://PROVIDER-ONE/test/getValue?i="+i;
        String re = template.getForObject(url,String.class);
        return re;
    }

    @Override
    @HystrixCommand(fallbackMethod = "addFallback")
    public String addForHystrix(Integer i) {
        String url = "http://PROVIDER-ONE/test/addForBreake?i="+i;
        String re = template.getForObject(url,String.class);
        return re;
    }

    public String addFallback(Integer i){
        return "出错啦呀";
    }



}
