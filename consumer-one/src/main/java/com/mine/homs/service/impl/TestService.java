package com.mine.homs.service.impl;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
public interface TestService {
    String add(Integer i);

    String addForHystrix(Integer i);
}
