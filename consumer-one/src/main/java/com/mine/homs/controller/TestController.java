package com.mine.homs.controller;

import com.mine.homs.service.impl.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sen.wang@hand-china.com
 * @description
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {
    @Autowired
    TestService testService;

    @GetMapping(value = "/getValue")
    public String add(Integer i){
        return testService.add(i);
    }
    @GetMapping(value = "/addForBreake")
    public String addForBreake(Integer i){
        return testService.addForHystrix(i);
    }


}
